import React from 'react';
import Header from 'components/Header'
// import Footer from 'components/Footer'
import TaskList from 'containers/TaskList'
import SingleTask from 'containers/SingleTask';
import history from 'utils/history'
import {Router, Switch, Route } from "react-router-dom";

import './app.scss'

function App() {
  return (
    <Router history={history}>
      <div className="App">
        <Header />
        <div className="wrapper">
          <Switch>
            <Route exact path="/" component={TaskList} />
            <Route path="/single-task" component={SingleTask} />
          </Switch>
        </div>
        {/* <Footer /> */}
      </div>
    </Router>
  );
}

export default App;
