import React from 'react';
import classNames from 'classnames'
import * as moment from 'moment'
import { Link } from 'react-router-dom'

import './styles.scss';

const TaskItem = (props) => {
  let expDate = moment(props.date)
  const itemClasses = classNames({
    'task-item': true,
    'one-day-left': moment().add(1, 'days').isSame(expDate, 'day'),
    'last-day': moment().isSame(expDate, 'day'),
    'expired': moment().isAfter(expDate, 'day')
  })

  let columns = props.columns.map(el => (<option key={el.id} value={el.title}>{el.title}</option>))

  return (
    <div onDoubleClick={() => props.deleteTodo(props.id)} className={itemClasses}>
      <div className="info">
        <select onChange={(e) => props.changeCategory(e.target.value, props.id)} defaultValue={props.category} name="" id="">
          {columns}
        </select>
        <h3 className="task-heading">{props.heading}</h3>
        <p>{props.text}</p>
        <span className="date-expires">{props.date}</span>
      </div>
      <Link to={{ pathname: '/single-task', state: { store: props.store, el: props.item } }}> EDIT </Link>
    </div>
  )
};

export default TaskItem;