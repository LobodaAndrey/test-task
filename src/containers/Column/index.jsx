import React, { Component } from 'react';
import TaskItem from 'components/TaskItem'
import { Button } from 'react-bootstrap'

import './styles.scss';

class Column extends Component {

  render() {
    const list = this.props.todos.filter(el => el.category === this.props.title).map(el => {
      return (
        <TaskItem store={this.props.store} changeCategory={this.props.changeCategory} columns={this.props.columns} handleShow={this.props.handleShow} parentTitle={this.props.title} deleteTodo={this.props.deleteTodo} id={el.id} key={el.id} history={this.props.history} 
        item={el}
        {...el }  />
      )
    })

    return (
      < div className="column-item">
        <div className="column-heading">
          <h2 onDoubleClick={() => this.props.editColumn(this.props.id)} >{this.props.title}</h2>
          <Button size="sm" onClick={() => {
            this.props.deleteColumn(this.props.id)
          }} variant="secondary">Delete column</Button>
        </div>
        {list}
      </div>
    );
  }
}

export default Column;