import React, { Component } from 'react'
import Column from 'containers/Column'
import { Form, Button } from 'react-bootstrap'
import './styles.scss'

class TaskList extends Component {
  state = {
    newTaskTitle: '',
    newTaskText: '',
    newTaskDate: '',
    newColumnTitle: '',
    editColumnTitle: '',
    show: false,
    // Строчка ниже - говнокод, но я не работал раньше с тем, чтобы переносить стейт приложения между роутами без использования редакса - пришлось придумывать костыль =)
    todos: (this.props.location.state !== undefined && this.props.location.state.todos) || [
      {
        id: 100,
        heading: 'Double Click',
        text: 'To delete task',
        date: '2019-11-25',
        category: 'New'
      },
      {
        id: 101,
        heading: 'Double Click',
        text: 'To delete task',
        date: '2019-10-30',
        category: 'New'
      },
      {
        id: 102,
        heading: 'Double Click',
        text: 'To delete task',
        date: '2019-10-29',
        category: 'Testing'
      },
      {
        id: 103,
        heading: 'Double Click',
        text: 'To delete task',
        date: '2019-10-25',
        category: 'New'
      },
      {
        id: 104,
        heading: 'Double Click',
        text: 'To delete task',
        date: '2019-10-25',
        category: 'Done'
      },
      {
        id: 105,
        heading: 'Double Click',
        text: 'To delete task',
        date: '2019-10-28',
        category: 'In progress'
      }
    ],
    columns: (this.props.location.state !== undefined && this.props.location.state.columns) || [
      {
        initial: true,
        id: 1,
        title: 'New',
      },
      {
        initial: false,
        id: 2,
        title: 'In progress',
      },
      {
        initial: false,
        id: 3,
        title: 'Testing',
      },
      {
        initial: false,
        id: 4,
        title: 'Done',
      }
    ]
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  addColumn = () => {
    let newItem = {
      id: Math.floor(Math.random() * 10000),
      title: this.state.newColumnTitle,
      todos: []
    }
    if (this.state.newColumnTitle.length < 2) {
      return alert(' You cant add empty column')
    }
    this.setState((prev) => {
      return {
        ...prev,
        columns: [...prev.columns, newItem],
        newColumnTitle: ''
      }
    })
  }

  addTask = () => {
    let newTask = {
      id: Math.floor(Math.random() * 10000),
      heading: this.state.newTaskTitle || 'heading',
      text: this.state.newTaskText || 'content',
      date: this.state.newTaskDate || '12-12-2019',
      category: 'New'
    }

    let { todos } = this.state;
    todos = [newTask, ...todos,]
    this.setState({
      todos,
      newTaskTitle: '',
      newTaskText: '',
      newTaskDate: ''
    })
  }

  editTask = () => {
    console.log('Edited')
    this.handleClose()
  }

  changeCategory = (value, id) => {
    const { todos } = this.state
    todos.map(el => {
      if (el.id === id) {
        el.category = value
      }
      return el
    })
    this.setState({
      todos
    })
  }

  editColumn = (id) => {
    const { columns } = this.state;
    const { todos } = this.state;
    let oldTitle = ''
    if (this.state.editColumnTitle.length < 1) {
      return alert(' You cant change title to empty')
    }

    columns.map(el => {
      if (el.id === id) {
        oldTitle = el.title
        el.title = this.state.editColumnTitle
      }
      return el
    })

    todos.map(el => {
      if (el.category === oldTitle) {
        return el.category = this.state.editColumnTitle
      }
      return el
    })

    this.setState({
      columns,
      todos,
      editColumnTitle: ''
    })
  }

  deleteColumn = (id) => {
    if (id === 1) {
      return alert('You are trying to delete root column, cant allow you to do it =(')
    }
    const newList = this.state.columns.filter(el => el.id !== id)
    this.setState({ columns: newList })
  }

  deleteTodo = (id) => {
    console.log('click')
    let { todos } = this.state;
    todos = todos.filter(el => el.id !== id)
    this.setState({
      todos
    })
  }

  render() {
    const columnsList = this.state.columns.map(el => {
      return (
        <Column changeCategory={this.changeCategory} columns={this.state.columns} handleShow={this.handleShow} editColumn={this.editColumn} todos={this.state.todos} deleteColumn={this.deleteColumn} deleteTodo={this.deleteTodo} id={el.id} key={el.id} store={this.state} {...el} />
      )
    })

    return (
      <>
        <div className="control-part">
          <div>
            <h3>Add new column</h3>
            <Form.Group >
              <Form.Control onChange={this.handleChange} value={this.state.newColumnTitle} name="newColumnTitle" maxLength="15" type="text" placeholder="" />
            </Form.Group>
            <Button size="sm" variant="primary" onClick={this.addColumn}>Add column</Button>
          </div>
          <div>
            <h3>Add new task</h3>
            <div className="add-task-form">
              <Form>
                <Form.Group >
                  <Form.Control name="newTaskTitle" onChange={this.handleChange} value={this.state.newTaskTitle} maxLength="15" type="text" placeholder="Title" />
                </Form.Group>

                <Form.Group>
                  <Form.Control name="newTaskText" onChange={this.handleChange} value={this.state.newTaskText} maxLength="100" as="textarea" placeholder="Text" />
                </Form.Group>

                <Form.Group >
                  <Form.Control name="newTaskDate" onChange={this.handleChange} value={this.state.newTaskDate} type="date" placeholder="Date expires" />
                </Form.Group>

                <Button variant="primary" onClick={this.addTask}> Add TASK </Button>
              </Form>
            </div>
          </div>
          <div>
            <h3>Edit column name</h3>
            <p style={{ color: 'white', maxWidth: 600, margin: "10px auto" }}>To edit, input new value in the field below and double click on header of column you want to change. E.G. on "Testing" or "New"</p>
            <Form.Group >
              <Form.Control type="text" onChange={this.handleChange} value={this.state.editColumnTitle} name="editColumnTitle" maxLength="15" placeholder="" />
            </Form.Group>
          </div>
        </div>

        <div className="columns-wrapper">
          {columnsList}
        </div>
      </>
    );
  }
}

export default TaskList;