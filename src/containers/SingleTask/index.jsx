import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'

import './styles.scss'

class SingleTask extends Component {
  state = {
    heading: this.props.location.state.el.heading || '',
    text: this.props.location.state.el.text || '',
    date: this.props.location.state.el.date || '',
    todos: this.props.location.state.store.todos || {},
    columns: this.props.location.state.store.columns
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  edit = () => {
    let id = this.props.location.state.el.id
    const {todos} = this.state
    todos.map(el => {
      if (el.id === id) {
        el.heading = this.state.heading;
        el.text = this.state.text;
        el.date = this.state.date
      }
      return el
    })
    this.setState({
      todos
    })
    this.props.history.push({
      pathname: '/',
      state: { todos: this.state.todos, columns: this.state.columns }
    })
  }

  render() {
    return (
      <>
        <Link to="/">Back</Link>
        <Form>
          <Form.Group >
            <Form.Control name="heading" onChange={this.handleChange} value={this.state.heading} maxLength="15" type="text" placeholder="Title" />
          </Form.Group>

          <Form.Group>
            <Form.Control name="text" onChange={this.handleChange} value={this.state.text} maxLength="100" as="textarea" placeholder="Text" />
          </Form.Group>

          <Form.Group >
            <Form.Control name="date" onChange={this.handleChange} value={this.state.date} type="date" placeholder="Date expires" />
          </Form.Group>

          <Button variant="primary" onClick={this.edit}> Edit </Button>
        </Form>
      </>
    );
  }
}

export default SingleTask;
